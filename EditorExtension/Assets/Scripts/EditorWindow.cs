﻿using UnityEditor;
using UnityEngine;

public class EditorWindowExtension: EditorWindow
{

    //string txt = "COLORIZE!";
    Color color;

    [MenuItem("Window/EditorWindow")]
    public static void ShowWindow()
    {

        GetWindow<EditorWindowExtension>("Colorize");

    }

    private void OnGUI()
    {
        GUILayout.Label("Color the selected objects", EditorStyles.boldLabel);

        color = EditorGUILayout.ColorField("COLOR", color);

        if (GUILayout.Button("COLORIZE"))
        {

            Colorize();

        }


    }

    void Colorize()
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            Renderer renderer = obj.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.sharedMaterial.color = color;
            }
        }
    }

}
