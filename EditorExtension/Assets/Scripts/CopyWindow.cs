﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CopyWindow : EditorWindow {

    string x = "0", y = "0", z = "0", times = "0";
    //int X = 0, Y = 0, Z = 0, Times = 0;
    //int iniX, iniY, iniZ;

    
    float X = 0, Y = 0, Z = 0, Times = 0;
    float iniX, iniY, iniZ;

    List<GameObject> objects = new List<GameObject>();

    [MenuItem("Window/CopyWindow")]
    public static void ShowWindow()
    {

        GetWindow<CopyWindow>("CopyWindow");

    }

    private void OnGUI()
    {
        GUILayout.Label("Copy the selected Objects", EditorStyles.boldLabel);

        x = EditorGUILayout.TextField("X Axis Displacement ", x);
        y = EditorGUILayout.TextField("Y Axis Displacement ", y);
        z = EditorGUILayout.TextField("Z Axis Displacement ", z);
        times = EditorGUILayout.TextField("Number of copies ", times);

        X = Convert.ToInt32(x);
        Y = Convert.ToInt32(y);
        Z = Convert.ToInt32(z);
        Times = Convert.ToInt32(times);

        if (GUILayout.Button("COPY"))
        {

            //Copy(X, Y, Z, objects, iniX, iniY, iniZ);

            foreach (GameObject obj in Selection.gameObjects)
            {

                Renderer renderer = obj.GetComponent<Renderer>();
                objects.Add(obj);

                for (int i = 0; i < Times; i++)
                {
                    if (objects.Count >= 1)
                    {
                        iniX = objects[objects.Count - 1].transform.position.x;
                        iniY = objects[objects.Count - 1].transform.position.y;
                        iniZ = objects[objects.Count - 1].transform.position.z;
                    }


                    if (renderer != null)
                    {
                        objects.Add(Instantiate(obj, new Vector3(iniX + X, iniY + Y, iniZ + Z), new Quaternion(0, 0, 0, 0)));

                    }

                }

            }

        }


    }

}


